stompcat(1) -- display output from an STOMP destination
========================================================

## SYNOPSIS

stompcat /topic/foo

## DESCRIPTION

This program will display the output that arrives at the /topic/foo stomp
destination.

## ENVIRONMENT

** STOMP_HOST ** Hostname or IP address of the STOMP server. If not set, "localhost" will be used.

** STOMP_PORT ** Port of the STOMP server. If not set, "61613" will be used.

** STOMP_USER ** Username of the STOMP server.

** STOMP_PASSWORD ** Password of the STOMP server.

## AUTHOR

LogicBlaze Inc

This manual page was written by Jonas Genannt <jonas.genannt@capi2name.de> for the Debian Project.
