catstomp(1) -- send input to STOMP destination
========================================================

## SYNOPSIS

ls | catstomp /topic/foo # Would send the output of "ls" to the STOMP destination "/topic/foo"

## DESCRIPTION

It allows you to send input into this process to STOMP destination.


## ENVIRONMENT

** STOMP_HOST ** Hostname or IP address of the STOMP server. If not set, "localhost" will be used.

** STOMP_PORT ** Port of the STOMP server. If not set, "61613" will be used.

** STOMP_USER ** Username of the STOMP server.

** STOMP_PASSWORD ** Password of the STOMP server.

## AUTHOR

LogicBlaze Inc

This manual page was written by Jonas Genannt <jonas.genannt@capi2name.de> for the Debian Project.
