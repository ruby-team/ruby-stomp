require "rspec/autorun"
$: << 'lib' << '.' << 'spec'
Dir['spec/*_spec.rb'].each { |f| require f }
